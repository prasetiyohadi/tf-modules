variable "app" {
  type    = string
  default = null
}

variable "cluster" {
  type    = string
  default = null
}

variable "delimiter" {
  type    = string
  default = null
}

variable "env" {
  type    = string
  default = null
}

variable "location" {
  type    = string
  default = null
}

variable "name" {
  type    = string
  default = null
}

variable "pic" {
  type    = string
  default = null
}

variable "prefix" {
  type    = string
  default = null
}

variable "resource_group" {
  type    = string
  default = null
}

variable "service" {
  type    = string
  default = null
}

variable "squad" {
  type    = string
  default = null
}

variable "tags" {
  type    = map(any)
  default = null
}

variable "tribe" {
  type    = string
  default = null
}
