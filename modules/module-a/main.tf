locals {
  prefix    = var.prefix == null ? "a_v0.0.1" : var.prefix
  delimiter = var.delimiter == null ? "-" : var.delimiter
  name      = var.cluster == null ? join(local.delimiter, [var.app, var.env]) : join(local.delimiter, [var.app, var.cluster, var.env])
  full_name = join(local.delimiter, [local.prefix, local.name])
  default_tags = {
    app      = var.app
    cluster  = var.cluster
    env      = var.env
    location = var.location
    name     = local.full_name
    pic      = var.pic
    region   = var.location
    service  = var.service
    squad    = var.squad
    tribe    = var.tribe
  }
  tags = merge(local.default_tags, try(var.tags, {}))
}
