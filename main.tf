locals {
  prefix    = var.prefix == null ? "root_v0.0.1" : var.prefix
  delimiter = var.delimiter == null ? "-" : var.delimiter
  name      = var.cluster == null ? join(local.delimiter, [var.app, var.env]) : join(local.delimiter, [var.app, var.cluster, var.env])
  full_name = join(local.delimiter, [local.prefix, local.name])
  default_tags = {
    app      = var.app
    cluster  = var.cluster
    env      = var.env
    location = var.location
    name     = local.full_name
    pic      = var.pic
    region   = var.location
    service  = var.service
    squad    = var.squad
    tribe    = var.tribe
  }
  tags = merge(local.default_tags, try(var.tags, {}))
}

module "module_a" {
  source = "./modules/module-a"

  app     = var.app
  cluster = var.cluster
  env     = var.env
  pic     = var.pic
  service = var.service
  squad   = var.squad
  tribe   = var.tribe
}

module "module_b" {
  source = "./modules/module-b"

  app     = var.app
  cluster = var.cluster
  env     = var.env
  pic     = var.pic
  service = var.service
  squad   = var.squad
  tribe   = var.tribe
}

module "module_c" {
  source = "./modules/module-c"

  app     = var.app
  cluster = var.cluster
  env     = var.env
  pic     = var.pic
  service = var.service
  squad   = var.squad
  tribe   = var.tribe
}
