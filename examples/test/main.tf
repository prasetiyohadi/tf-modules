module "tf_modules" {
  source = "../../"

  app     = var.app
  cluster = var.cluster
  env     = var.env
  pic     = var.pic
  service = var.service
  squad   = var.squad
  tribe   = var.tribe
}

output "tags" {
  value = module.tf_modules.tags.name
}

output "module_a_tags" {
  value = module.tf_modules.module_a_tags.name
}

output "module_b_tags" {
  value = module.tf_modules.module_b_tags.name
}

output "module_c_tags" {
  value = module.tf_modules.module_c_tags.name
}
