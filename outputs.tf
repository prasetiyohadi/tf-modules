output "module_a_tags" {
  value = module.module_a.tags
}

output "module_b_tags" {
  value = module.module_b.tags
}

output "module_c_tags" {
  value = module.module_c.tags
}

output "tags" {
  value = local.tags
}
